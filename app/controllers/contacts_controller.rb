class ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end
  
  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      name = params[:contact][:name]
      email = params[:contact][:email]
      body = params[:contact][:comments]
      
      ContactMailer.contact_email(name,email,body).deliver
      # Need to set up email functionality on Heroku and set up heroku
      flash[:success] = "Message Sent"
      redirect_to root_url
    else
      flash[:danger] = "Error Occured"
      redirect_to new_contact_path
    end
  end
    
end
