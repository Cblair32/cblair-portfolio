class ContactMailer < ActionMailer::Base
  default to: "cameronblair@outlook.com"
   
  def contact_email
    @name = name
    @email = email
    @body = body
    
    mail(from: email, subject: "Message from #{@name}. Sent from your portfolio")
  end
end