require 'test_helper'

class LandingsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get landings_index_url
    assert_response :success
  end
  
  
  test "Should get root" do
    get root_path
    assert_response :success
  end

end
